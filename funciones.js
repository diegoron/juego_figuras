var puntuacion = 0;

function drag(event){
    event.dataTransfer.setData("text",event.target.id);
}

function drop(event){
    puntuacion = puntuacion + 1;
    event.preventDefault();
    var data = event.dataTransfer.getData("text");
    event.target.appendChild(document.getElementById(data));
    document.getElementById("resultado").value = puntuacion;
    if(puntuacion == 10){
        alert("Has ganado el juego");
        document.getElementById("resultado").value = 0;
        puntuacion = 0;
    }
}

function allowdrop(event){
    event.preventDefault();
}